package com.example.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.example.domain.EmailHash;

@Service
public class MailSenderService {

	@Autowired
	private Environment environment;
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private SpringTemplateEngine templateEngine;
	@Autowired
	private MessageSourceService messageSourceService;
	@Autowired
	private RequestHelperService requestHelperService;

	private String from;

	@PostConstruct
	public void postConstruct() {
		String fromMail = environment.getProperty("application.mail.from");
		from = messageSourceService.getMessage("application.mail.from", fromMail);
	}

	/**
	 * Send email
	 */
	public void sendMail(String from, String to, String subject, String template, Map<String, ?> variables)
			throws MessagingException {

		// Prepare the evaluation context
		HttpServletRequest request = requestHelperService.getRequest();
		WebContext ctx = new WebContext(request, null, request.getServletContext(), LocaleContextHolder.getLocale(), variables);

		// Prepare message using a Spring helper
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper message = new MimeMessageHelper(mimeMessage, false, "UTF-8");
		message.setFrom(from);
		message.setTo(to);
		message.setSubject(subject);

		// Create the HTML body using Thymeleaf
		String htmlContent = templateEngine.process(template, ctx);
		message.setText(htmlContent, true); // true = isHtml

		// Send mail
		mailSender.send(mimeMessage);
	}

	/**
	 * Send email with image
	 */
	public void sendMailWithInline(String from, String to, String subject, String template, Map<String, ?> variables,
			String imageResourceName, byte[] imageBytes, String imageContentType) throws MessagingException {

		// Prepare the evaluation context
		Context ctx = new Context(LocaleContextHolder.getLocale(), variables);

		// Prepare message using a Spring helper
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8"); // true = multipart
		message.setFrom(from);
		message.setTo(to);
		message.setSubject(subject);

		// Create the HTML body using Thymeleaf
		String htmlContent = templateEngine.process(template, ctx);
		message.setText(htmlContent, true); // true = isHtml

		// Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
		InputStreamSource imageSource = new ByteArrayResource(imageBytes);
		message.addInline(imageResourceName, imageSource, imageContentType);

		// Send mail
		mailSender.send(mimeMessage);
	}

	/**
	 * 
	 * @param emailHash
	 * @param subjectCode
	 * @param template
	 * @throws MessagingException
	 */
	public void sendHashMail(EmailHash emailHash, String subjectCode, String template) throws MessagingException {
		Map<String, Object> variables = new HashMap<>();
		variables.put("hash", emailHash.getHash());
		variables.put("baseUrl", requestHelperService.getBaseUrl());

		String to = emailHash.getEmail();
		String subject = messageSourceService.getMessage(subjectCode);
		this.sendMail(from, to, subject, template, variables);
	}

}
