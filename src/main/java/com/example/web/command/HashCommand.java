package com.example.web.command;

import javax.validation.constraints.Pattern;

import com.example.util.WebUtils;

public class HashCommand {

	@Pattern(regexp = WebUtils.REGEX_HASH_PATTERN)
	private String hash;

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	@Override
	public String toString() {
		return "HashCommand [hash=" + hash + "]";
	}

}
