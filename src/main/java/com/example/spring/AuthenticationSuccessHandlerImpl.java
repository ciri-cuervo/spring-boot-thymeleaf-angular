package com.example.spring;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import com.example.domain.types.RoleAuthority;

public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

    private Log log = LogFactory.getLog(AuthenticationSuccessHandlerImpl.class);

    private final static String DEFAULT_PATH = "/";

    private final RequestCache requestCache;
    private final RedirectStrategy redirectStrategy;

    public AuthenticationSuccessHandlerImpl() {
        this.requestCache = new HttpSessionRequestCache();
        this.redirectStrategy = new DefaultRedirectStrategy();
    }

    protected void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }

    protected RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }

	protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {

		if (response.isCommitted()) {
			log.debug("Response has already been committed.");
			return;
		}

		SavedRequest savedRequest = requestCache.getRequest(request, response);
		if (savedRequest == null) {
			clearAuthenticationAttributes(request);
			redirectStrategy.sendRedirect(request, response, DEFAULT_PATH);
			return;
		}
		if (savedRequest.getMethod().equalsIgnoreCase("get")) {
			redirectStrategy.sendRedirect(request, response, savedRequest.getRedirectUrl());
		} else {
			redirectStrategy.sendRedirect(request, response, DEFAULT_PATH);
		}
	}

    protected boolean isAdmin(Authentication authentication) {
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        for (GrantedAuthority grantedAuthority : authorities) {
            if (grantedAuthority.getAuthority().equals(RoleAuthority.ADMIN.toString())) {
                return true;
            }
        }
        return false;
    }

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException {
		//handle(request, response, authentication);
		//clearAuthenticationAttributes(request);
		
		// emailHashRepository.deactivateByEmail(user.getEmail());
	}

}
