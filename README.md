# Spring Boot with Thymeleaf and AngujarJS #

This is a demo webapp used as portfolio.

### Technologies ###

* Java 8
* Spring Boot (Web MVC, Security, Data-JPA)
* Hibernate (JPA and Validation)
* Thymeleaf
* AngularJS
* Mockito
* WebJars (Bootswatch Yeti Theme, Bootstrap, jQuery, Font Awesome)
* Maven

### How do I get set up? ###

+ Download source code
    * `git clone https://bitbucket.org/ciri-cuervo/spring-boot-thymeleaf-angular.git`
+ Summary of set up
    * Import as Maven Project in your favorite IDE (Eclipse - IntelliJ IDEA)
* Configuration
* Dependencies
+ Database configuration
    * Set DB name and connection parameters in file `application-prod.properties` under `src/main/resources`
* How to run tests
+ Deployment instructions
    * Just run `Application.java` as a Java application :)
    * It will be accessible at: `http://localhost:9090/`
